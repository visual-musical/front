import React from "react";
import axios from "axios/index";
import Camera from '../../components/Camera'

class CameraContainer extends React.Component {
    constructor() {
        super();
        this.state = {
            devices: []
        };
    }

    static sendColor(rgb, duration, id) {
        axios.get(`http://192.168.43.186:3000/devices/${id}/color`, {
            params: {r: rgb.r, g: rgb.g, b: rgb.b, duration: duration, id: id}
        });
    }

    componentDidMount() {
        this.getDevices();
    }

    getDevices() {
        axios.get('http://192.168.43.186:3000/devices/list')
            .then(result => {
                this.setState({
                    devices: result.data
                });
            });
    }

    render() {
        return <Camera
            sendColor={CameraContainer.sendColor}
            getDevices={this.getDevices}
            devices={this.state.devices}
        />
    }
}

export default CameraContainer;