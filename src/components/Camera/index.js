import React from "react";
import './index.css'

class Camera extends React.Component {
    constructor(props) {
        super(props);
        this.video = React.createRef();
        this.visualizer = React.createRef();
        this.state = {
            deviceId: "",
            duration: 100
        };
    }

    static getAverageRGB(bitmap) {
        const blockSize = 5;
        const defaultRGB = {r: 0, g: 0, b: 0};
        const canvas = document.createElement('canvas');
        const context = canvas.getContext && canvas.getContext('2d');
        const rgb = {r: 0, g: 0, b: 0};
        let data;
        let i = -4;
        let count = 0;

        if (!context) {
            return defaultRGB;
        }

        const height = canvas.height = bitmap.naturalHeight || bitmap.offsetHeight || bitmap.height;
        const width = canvas.width = bitmap.naturalWidth || bitmap.offsetWidth || bitmap.width;

        context.drawImage(bitmap, 0, 0);

        try {
            data = context.getImageData(0, 0, width, height);
        } catch (e) {
            return defaultRGB;
        }

        const length = data.data.length;

        while ((i += blockSize * 4) < length) {
            ++count;
            rgb.r += data.data[i];
            rgb.g += data.data[i + 1];
            rgb.b += data.data[i + 2];
        }

        rgb.r = ~~(rgb.r / count);
        rgb.g = ~~(rgb.g / count);
        rgb.b = ~~(rgb.b / count);

        return rgb;
    }

    stop() {
        clearInterval(this.capture);
        this.capture = null;
    }

    launch() {
        const duration = this.state.duration;
        if (!this.capture) {
            this.takePicture(duration);
            this.capture = setInterval(this.takePicture.bind(this, duration), duration);
        }
    }

    componentDidMount() {
        this.processDevices();
    }

    handleSuccess(stream) {
        this.video.current.srcObject = stream;
        this.stream = stream;
    }

    takePicture(duration) {
        if (this.stream) {
            const image = new ImageCapture(this.stream.getVideoTracks()[0]);
            if (image) {
                image.grabFrame().then(bitmap => {
                    this.processPicture(bitmap, duration);
                }).catch(error => {
                    if (error)
                        console.log(error.message);
                });
            }
        }

    }

    processPicture(bitmap, duration) {
        const {sendColor} = this.props;
        const color = Camera.getAverageRGB(bitmap);
        console.log(color)
        this.visualizer.current.style.background = `rgb(${color.r}, ${color.g}, ${color.b})`;
        sendColor(color, duration, this.state.deviceId)
    }

    processDevices() {
        navigator.mediaDevices.enumerateDevices()
            .then((devices) => devices.filter((d) => d.kind === 'videoinput'))
            .then((all_devices) => {
                const deviceId = all_devices[all_devices.length - 1].deviceId;
                if (all_devices) {
                    navigator.mediaDevices.getUserMedia({
                        audio: false,
                        video: {
                            deviceId: deviceId
                        }
                    })
                        .then(this.handleSuccess.bind(this))
                        .catch(error => {
                            if (error)
                                console.log(error.message);
                        });
                }
            });
    }

    handleChange(event) {
        this.setState({deviceId: event.target.value});
    }

    render() {
        const {devices} = this.props;
        return <div className="camera-container">
            <div className="color" ref={this.visualizer}/>
            <video className="video" ref={this.video} autoPlay/>
            <select onChange={this.handleChange.bind(this)}>
                <option value="">Choose your device</option>
                {devices && devices.map(device => <option key={device} value={device}>{device}</option>)}
            </select>
            <div className="button-box">
                {this.state.deviceId && devices && devices.length > 0 && (
                    <>
                        <button onClick={this.launch.bind(this)}>Start</button>
                        <button onClick={this.stop.bind(this)}>Stop</button>
                    </>
                )}
            </div>
        </div>;
    }
}

export default Camera;