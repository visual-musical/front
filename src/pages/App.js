import React from 'react';
import Camera from '../container/Camera';
import './App.css'

const App = () => (
    <>
        <p className="title">Welcome to Visual Musical</p>
        <Camera/>
    </>
);

export default App;